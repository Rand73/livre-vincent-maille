from typing import Dict

from tools_tkinter import Window as Win, Canvas as Can, Frame as Fram, Button as But, PhotoImage
from tkinter.messagebox import showwarning
import tkinter


class Window(Win):

    def __init__(self):
        super(Window, self).__init__(width=800, height=600)
        self.resizable(width=False, height=False)
        self.title("Coccinelle")


class Frame(Fram):

    def __init__(self, master, width: int, height: int, place_x: int, place_y: int, bg=''):
        super(Frame, self).__init__(master=master, width=width, height=height, bg=bg)
        self.pack_propagate(0)
        self.place(x=place_x, y=place_y)


class Canvas(Can):
    __canvas_bg: str = "white"
    __width: int = 500
    __height: int = 500
    __x: int = 50
    __y: int = 50

    def __init__(self, master):
        self.__frame = Frame(master=master,
                             width=self.__width,
                             height=self.__height,
                             place_x=self.__x,
                             place_y=self.__y)

        super(Canvas, self).__init__(master=self.__frame,
                                     bg=self.__canvas_bg)

        self.pack(fill=tkinter.BOTH, expand=1)
        pass

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    def showwarning(self):
        showwarning(title='Dépassement de zone',
                    message="Attention, vous essayer de sortir de l'écran")
        pass


class DirectionButtons(But):
    __width: int = 100
    __height: int = 50
    __canvas: Canvas
    __direction: str
    __image: PhotoImage = None
    __tags = ''
    __dict_direction: Dict[str, tuple] = {'Haut': (0, -10),
                                          'Bas': (0, 10),
                                          'Gauche': (-10, 0),
                                          'Droite': (10, 0)}

    def __init__(self, master, place_x: int, place_y: int, canvas: Canvas, image: PhotoImage, tags='', cnf={}, **kw):
        self.__frame = Frame(master=master,
                             width=self.__width,
                             height=self.__height,
                             place_x=place_x,
                             place_y=place_y)

        self.canvas = canvas
        self.__direction = kw['text']
        if isinstance(image, PhotoImage):
            self.__image = image

        self.__tags = tags
        super(DirectionButtons, self).__init__(master=self.__frame,
                                               command=self.on_click,
                                               cnf=cnf,
                                               **kw)
        self.pack(fill=tkinter.BOTH, expand=1)
        pass

    @property
    def canvas(self):
        return self.__canvas

    @canvas.setter
    def canvas(self, canvas: Canvas):
        if isinstance(canvas, Canvas):
            self.__canvas = canvas
        pass

    def on_click(self, event=None):
        self.canvas.itemconfigure(self.__tags, image=self.__image)
        x, y = self.__dict_direction[self.__direction]
        x1, y1 = self.canvas.coords(self.__tags)
        x1 += x
        y1 += y
        width_max = self.canvas.width - self.__image.width() + 1
        height_max = self.canvas.height - self.__image.height() + 1
        if x1 in range(width_max) and y1 in range(height_max):
            self.canvas.move(self.__tags, x, y)
        self.canvas.update()
        pass
