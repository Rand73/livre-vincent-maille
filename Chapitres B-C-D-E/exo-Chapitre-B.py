print("hello")
# age = eval(input("Entre ton age"))
# print(age)
table = 0
while table <= 90:
    print(table)
    table += 9
a1a: int = 0
a1b: int = 1
a1a += a1b
a1b *= a1a
a1a -= 2
print("Resultat A1 : a=" + str(a1a) + ", b=" + str(a1b))
a = 7
b = 21
if a < b:
    print("Résultat de l'exo A2 et : " + str(b))
else:
    print("Résultat de l'exo A2 et : " + str(a))

a = 14
b = 3
while b <= a:
    a -= b
print("Résultat de l'exo A3 et : " + str(a))

print("Exo B3")
IMC_MAX = 25.0
IMC_MIN = 18.5
taille: float = 1.81  # eval(input("Entre ta taille : "))
poidMin = IMC_MIN * taille ** 2
poidMax = IMC_MAX * taille ** 2
print("Votre poid idéal se situe entre {0}kg et {1}kg.".format(str(poidMin), str(poidMax)))

print("Exo B4")
taille = 1.81  # eval(input("Entre ta taille : "))
poid: float = 69  # eval(input("Entre ton poid : "))
imc = poid / (taille ** 2)
print("Votre IMC est de {0}.".format(str(imc)))
if imc > IMC_MAX:
    print("Il serait bon de perdre du poid votre IMC est suppérieur à l'IMC Maximum conseillé.")
elif imc < IMC_MIN:
    print("Il serait bon de prendre du poid votre IMC est inférieur à l'IMC Minimum conseillé.")
else:
    print("Votre IMC est dans la fourchette conseillée. Bien joué!!")

print("Exo B5 : baballe")
pixelChute: int = 400
nbRebond: int = 0
while pixelChute > 5:
    nbRebond += 1
    pixelChute = pixelChute * 0.9
    print(pixelChute)
print('Nombre de rebonds de la baballe : {0}'.format(str(nbRebond)))

print("Exo B6 : Distributeur de billet")


somme:int = eval(input("Quelle somme voulez-vous retirer?"))
reste = somme
if somme%10 != 0:
    print("Il faut entrer une somme multiple de 10")
    exit()
billetCinqcent: int = 0
if somme % 500 == 0:
    billetCinqcent = somme / 500
else:
    billetCinqcent = (somme - (somme%500)) / 500
print("Voici ",billetCinqcent, " billets de 500 Euro")
somme = somme - (billetCinqcent * 500)

billetDeuxcent: int = 0
if somme % 200 == 0:
    billetDeuxcent = somme / 200
else:
    billetDeuxcent = (somme - (somme%200)) / 200
print("Voici ",billetDeuxcent, " billets de 200 Euro")
somme = somme - (billetDeuxcent * 200)

billetCent: int = 0
if somme % 100 == 0:
    billetCent = somme / 100
else:
    billetCent = (somme - (somme%100)) / 100
print("Voici ",billetCent, " billets de 100 Euro")
somme = somme - (billetCent * 100)

billetCinquante: int = 0
if somme % 50 == 0:
    billetCinquante = somme / 50
else:
    billetCinquante = (somme - (somme%50)) / 50
print("Voici ",billetCinquante, " billets de 50 Euro")
somme = somme - (billetCinquante * 50)

billetVingt: int = 0
if somme % 20 == 0:
    billetVingt = somme / 20
else:
    billetVingt = (somme - (somme%20)) / 20
print("Voici ",billetVingt, " billets de 20 Euro")
somme = somme - (billetVingt * 20)

billetDix: int = 0
if somme % 10 == 0:
    billetDix = somme / 10
else:
    billetDix = (somme - (somme%10)) / 10
print("Voici ",billetDix, " billets de 10 Euro")
somme = somme - (billetDix * 10)

print("Avec boucle for")
for i in (500, 200, 100, 50, 20, 10):
    n = reste // i
    print("Je vous donne ", n, " billets de ", i, " Euro")
    reste = reste - i * n
