import math, random
totalGrains = 0
grain = 1
for i in range(64):
    print(i)
    totalGrains += grain
    grain *= 2
print(totalGrains)

n1 = 12/5
n2 = 12/3
n3 = 12.1//2
n4 = 12//5
print(n1, type(n1))
print(n2, type(n2))
print(n3, type(n3))
print(n4, type(n4))

age = 1956 #eval(input("Quel est l'année de naissance de Guido Van Rossum à 5ans pres"))
if (age > 1961) and (age < 1951):
    print("Perdu c'est 1956")
else:
    print("Bravo c'est 1956 exactement")

i = 2 #eval(input("Précision de racine de 2 en chiffres après la virgule? "))
print(round(math.sqrt(2), i))

n=0
for i in range(10):
    a = 14 #eval(input("Entre un entier positif"))
    if math.gcd(a, 7) == 7:
        n += 1
print("Vous avez entrer {0} nombres divisile par 7.".format(str(n)))

n = 1988 #eval(input("Entre une année :"))
if (n % 4 == 0):
    if not(n % 100 == 0) or (n % 400 == 0):
        print("Vous avez entrer une année bissextille")

print("Exo C8")
IMC_MAX = 25.0
IMC_MIN = 18.5
poid = 68 #eval(input("Entre ta taille : "))
tailleMax = math.sqrt(poid / IMC_MIN)
tailleMin = math.sqrt(poid / IMC_MAX)
print("Votre taille idéal se situe entre {0}m et {1}m.".format(str(tailleMax), str(tailleMin)))

print("Exo C9")
n = 4 #eval(input("Parie en combien de coup de dé on obtien 6 :"))
a = 0
x = 0
while x != 6:
    a += 1
    x = random.randint(1, 6)
    print(x)
if a == n:
    print("Bravo les dés on été lancés ", a, " fois")
else:
    print("Loupé les dés on été lancés ", a, " fois")

print("Exo C10")
x = random.randint(1, 3)
n = 2 #eval(input("Pierre[1], Feuille[2], Ciseaux[3]?"))
if x == 1:
    if n == 1:
        print("Egalité")
    if n == 2:
        print("Le joueur gagne")
    if n == 3:
        print("L'ordinateur gagne")

if x == 2:
    if n == 1:
        print("L'ordinateur gagne")
    if n == 2:
        print("Egalité")
    if n == 3:
        print("Le joueur gagne")

if x == 3:
    if n == 1:
        print("Le joueur gagne")
    if n == 2:
        print("L'ordinateur gagne")
    if n == 3:
        print("Egalité")

n = 1
while n != 0:
    n = eval(input("Entre le nombre de dés que tu veux lancer pour faire 21 ou moins. Si tu veux arreter lance 0 dé."))
    if n == 0:
        break
    joueur = 0
    for i in range(n):
        joueur = joueur + random.randint(1, 6)
    print("Vous avez obtenu ", joueur, " points")
    bank = 0
    for i in range(n):
        bank += random.randint(1, 6)
    print("La bank à obtenu ", bank, " points")

    if joueur > 21 and bank > 21:
        print("Partie null")
        continue
    if joueur > 21 and bank <= 21:
        print("La bank gagne")
        continue
    if joueur <= 21 and bank > 21:
        print("Le joueur gagne")
        continue
    if joueur > bank:
        print("Le joueur gagne")
        continue
    else:
        print("La bank gagne")

