# Exo D1


belote = "Belote rebelote et dix de der !".lower()
carac = 'o'  # input("""Entrez une lettre à compter dans la phrase : '""" + belote + """'.""")

print(belote.count(carac))

# Exo D2

word = 'abgs'  # input("Ecrit un mot :?")
print(word[::-1])

# Exo D3

sentence = 'engage le jeu que je le gagne'  # input("Entrez un mot ou une phrase, je vérifierais si c'est un palindrome :?").lower()
pal = sentence.replace(' ', '')

if pal == pal[::-1]:
    print("Cette phrase '{0}' est un palindrome".format(sentence))
else:
    print("Cette phrase '{0}' n'est pas un palindrome".format(sentence))


# Exo D4
def doubleVoyelles(word):
    voyelles = 'aeiouy'
    ret = ''
    for i in word:
        for y in voyelles:
            if i == y:
                ret = ret + y
        ret = ret + i
    return ret


word = 'bonjour'  # input("Entre un mot que je double ses voyelles !!! ?")
print(doubleVoyelles(word))

# Exo D5
sentence = 'La brosse à dents bleue !'  # input("Entrez un mot ou une phrase :?")
pal = sentence.replace('', '*')
print(pal)


# Exo D6

def codage(sentence, word):
    ret = ''
    comp = 0
    for i in sentence:
        ret = ret + i
        ret = ret + word[comp % len(word)]
        comp += 1
    return ret


sentence  # = input("Entre une phrase :?")
word = 'serpent'  # input("Entre un mot de code pour ta phrase :?")

print(codage(sentence, word))


# Exo D7

def decode(sentense):
    ret = ''
    comp = 0
    for i in sentence:
        if comp % 2 == 0:
            ret = ret + i
        comp += 1
    return ret


sentence = codage(sentence, word)
print(decode(sentence))


# Exo D8

print("**********  JEU DU PENDU !  ***********")

word = "Salut" #input("Joueur 1: Entrez un mot :?")
carac = 'l' #input('Joueur 2: Entrez une lettre :?')
print("La lettre {0} apparait {1} fois dans le mot entré par le Joueur 1.".format(carac, word.count(carac)))

# Exo D9

import random

def melange(word):
    ret = ''
    comp = 0

    for l in word:
        if comp % 2 == 0:
            ret = ret + l
        else:
            ret = l + ret
        comp += 1
    return ret

word = input("Joueur 1 : Entre un mot :?")

word2 = input("Joueur 2: Devine le mot que le joueur 1 à entré : voici toutes les lettres mélangées : '{0}' :?".format(melange(word)))
if word == word2:
    print("Bravo!!")
else:
    print("Le mot à découvrir était : '{0}'".format(word))
