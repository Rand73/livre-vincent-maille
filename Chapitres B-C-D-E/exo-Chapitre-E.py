from random import randint

file = open("../fichier/sauvegarde.txt", 'r')
for i in range(3):
    l: str = file.readline().replace('\n', '')
    print(l)
file.close()

file = open("../fichier/carre.txt", 'w')
for i in range(10000):
    file.write(str(i ** 2))
    if i == 9999:
        continue
    file.write('\n')
file.close()

# Exo E1
file = open("../fichier/sauvegarde.txt", 'r')
print("Bonjour {0}, il vous reste {1} vies et vous êtes actuellement niveau {2}.".format(
    file.readline().replace('\n', ''), file.readline().replace('\n', ''), file.readline().replace('\n', '')))
file.close()

# Exo E2
file = open("../fichier/sauvegarde.txt", 'r')
nom = file.readline()
vie = file.readline()
niveau = eval(file.readline().replace('\n', '')) + 1
file.close()
file = open("../fichier/sauvegarde.txt", 'w')
file.write(nom)
file.write(vie)
file.write(str(niveau))
file.close()

# Exo E3
file = open("../fichier/liste_complete.txt", 'r')
mots: int = 0
while True:
    line = file.readline()
    if not line == "":
        mots += 1
        continue
    break
file.close()
print(f'Il y a {mots} mots dans ce fichier.')

# Exo E4
file = open("../fichier/liste_complete.txt", 'r')
mot = ''
while True:
    line = file.readline().replace('\n', '')
    if line == "":
        break
    if len(line) > len(mot):
        mot = line
file.close()
print(f"Le mot le plus long du fichier 'liste_complete.txt' est '{mot}'.")

# Exo E5
mots = []
file = open("../fichier/liste_complete.txt", 'r')
while True:
    line: str = file.readline().replace('\n', '')
    if line == "":
        break
    if len(line) == 5:
        mots.append(line)
file.close()
file = open("../fichier/mot5.txt", 'w')
for mot in mots:
    if mot == mots[-1]:
        file.writelines(mot)
        break
    file.writelines(mot + '\n')
file.close()
print("Création et écriture du fichier 'mot7.txt' terminée")

# Exo E6
file = open("../fichier/liste_complete.txt", 'r')
mots = file.readlines()
file.close()

nombre = randint(1, len(mots))
mot = mots[nombre].replace('\n', '')
print(f"Voici un mot au hasard : '{mot}', c'est le n°{nombre} du fichier 'liste_complete.txt'")
