"""Test recupéré sur un site"""

import tkinter


class Cercle(tkinter.Canvas):
    """Dessine un cercle. Le widget echelle fait varier le rayon"""

    def __init__(self, master=None, largeur=600, hauteur=300):
        tkinter.Canvas.__init__(self)
        self.largeur = largeur
        self.hauteur = hauteur
        self.configure(width=largeur, height=hauteur, highlightthickness=0)

        # Le rayon initial du cercle
        self.rayon = tkinter.IntVar()
        self.rayon.set(40)

        # Le cercle
        self.cercle = self.create_oval(largeur / 2 - self.rayon.get(),
                                       hauteur / 2 - self.rayon.get(), largeur / 2 + self.rayon.get(),
                                       hauteur / 2 + self.rayon.get())

        # Le widget echelle permet de faire varier le rayon du cercle
        self.echelle = tkinter.Scale(master, width=15, length=200,
                                     orient="horizontal", resolution=10, from_=10, to=120,
                                     variable=self.rayon, command=self.changer_cercle)

        # Les axes
        self.create_line(largeur / 2, 5, largeur / 2, hauteur - 5, fill="grey")
        self.create_line(5, hauteur / 2, largeur - 5, hauteur / 2, fill="grey")

        # Placement
        self.pack()
        self.echelle.pack(pady=3)

    def changer_cercle(self, pif):
        """Fonction associée au mouvement du widget échelle. La valeur affiché
        par celui-ci sera le rayon du cercle dessiné"""
        pif = int(pif)
        self.coords(self.cercle, self.largeur / 2 - pif,
                    self.hauteur / 2 - pif, self.largeur / 2 + pif,
                    self.hauteur / 2 + pif)


if __name__ == "__main__":
    fenetre = tkinter.Tk()
    c = Cercle(master=fenetre)
    fenetre.mainloop()
