from tools_tkinter import Window, Canvas, Font
from tkinter import PhotoImage

from random import randint

if __name__ == '__main__':

    width = 1000
    height = 500
    canvas_bg = 'blue'

    win = Window(width=width, height=height)
    win.title('Bataille de boules de neige')

    can = Canvas(master=win, width=width, height=height, bg=canvas_bg)
    can.pack()

    # Création des lignes
    x1 = 0  # Point tout à gauche.
    x2 = width / 2  # Point au centre.
    x3 = width  # Point tout à droite.
    for i in range(0, 50, 5):
        y1 = i * 2
        y2 = i
        y3 = y1
        _ = can.create_line(x1, y1, x2, y2, fill='white')
        _ = can.create_line(x2, y2, x3, y3, fill='white')

    font = Font(family='helvetica', size=40)
    text = can.create_text(x2, 100, fill='yellow', text='Bataille de boules de neige !', font=font)
    img = PhotoImage(file="noel.gif")
    can.create_image(500, 300, image=img, anchor='c')
    for x in range(width):
        for y in range(height):
            rand = randint(0, 500000)
            if 100 > rand > 10:
                d = randint(5, 10)
                _ = can.create_oval(x - d, y - d, x + d, y + d, fill='white', outline='white')
                # _ = can.create_text(x, y, fill='yellow', text='d={0} x={1} y={2}'.format(d, x, y),
                #                   font=('helvetica', d))
            if rand == 55:
                can.create_image(x, y, image=img)

    win.mainloop()
