from telecran_pack import Window, Canvas, DirectionButtons, ColorButtons


if __name__ == '__main__':
    win = Window()
    canvas = Canvas(master=win)

    up = DirectionButtons(win, place_x=625, place_y=350, canvas=canvas, text='Haut')
    down = DirectionButtons(win, place_x=625, place_y=450, canvas=canvas, text='Bas')
    left = DirectionButtons(win, 575, 400, canvas, text='Gauche')
    right = DirectionButtons(win, 675, 400, canvas, text='Droite')

    black = ColorButtons(win, 600, 60, 'black', canvas)
    white = ColorButtons(win, 700, 60, 'white', canvas)
    blue = ColorButtons(win, 600, 120, 'blue', canvas)
    red = ColorButtons(win, 700, 120, 'red', canvas)
    yellow = ColorButtons(win, 600, 180, 'yellow', canvas)
    green = ColorButtons(win, 700, 180, 'green', canvas)

    canvas.focus_set()
    canvas.bind("<Up>", up.on_click)
    canvas.bind("<Down>", down.on_click)
    canvas.bind("<Right>", right.on_click)
    canvas.bind("<Left>", left.on_click)

    win.mainloop()
