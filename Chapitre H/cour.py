from tools_tkinter import *
from time import sleep

if __name__ == '__main__':
    win = Window(width=200, height=200)
    win.title('Chapitre H')
    can = Canvas(win, width=200, height=200, bg='red')
    can.pack()

    # Création d'une ligne.
    line = can.create_line(0, 0, 200, 200, fill='green')

    # Création d'un rectangle.
    rect = can.create_rectangle(50, 50, 150, 150, fill='white')

    # Création d'une éllipse.
    ellipse = can.create_oval(50, 50, 150, 150, fill='blue')

    # Création d'un texte.
    text = can.create_text(100, 50, fill='yellow', text='Hello, world !', anchor='s')

    win.mainloop()
