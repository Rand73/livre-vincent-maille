from files import ReadFile
from typing import Dict
from tools_tkinter import PhotoImage, Window, Canvas

if __name__ == "__main__":
    # Init vars
    file = ReadFile(path='niveau.txt')
    level = file.read_to_array()

    height: int = 40 * len(level)
    width: int = 40 * len(level[0])

    win = Window(width=width, height=height)
    win.title('Exo 6')

    img_x: PhotoImage = PhotoImage(file='acier.gif')
    img_t: PhotoImage = PhotoImage(file='brique.gif')
    img_cat: PhotoImage = PhotoImage(file='chat.gif')
    img_h: PhotoImage = PhotoImage(file='echelle.gif')
    img_p: PhotoImage = PhotoImage(file='pancake.gif')
    img_mouse: PhotoImage = PhotoImage(file='souris.gif')

    img_dict: Dict[str, PhotoImage] = {'X': img_x,
                                       'T': img_t,
                                       'C': img_cat,
                                       'H': img_h,
                                       'P': img_p,
                                       'S': img_mouse, }

    can = Canvas(win, width=width, height=height)
    can.pack()

    for i, line in enumerate(level):
        # print(line)
        for j, letter in enumerate(line):
            if letter not in img_dict.keys():
                continue
            can.create_image(j * 40, i * 40, image=img_dict.get(letter), anchor='nw')

    win.mainloop()
    pass
