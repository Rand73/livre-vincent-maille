from tools_tkinter import Window, Canvas, PhotoImage

if __name__ == "__main__":
    win = Window(width=400, height=300)
    win.title('Exo_5')
    can = Canvas(master=win, width=400, height=300, bg='black')
    can.pack()
    img_boule = PhotoImage(file="boule.gif")
    img_fond = PhotoImage(file="fond.gif")
    img_pont = PhotoImage(file="pont.gif")
    img_pot = PhotoImage(file="pot.gif")

    can.create_image(200, 150, image=img_fond)
    can.create_image(100, 100, image=img_boule)
    can.create_image(200, 200, image=img_pont)
    can.create_image(350, 200, image=img_pot)
    win.mainloop()
    pass
