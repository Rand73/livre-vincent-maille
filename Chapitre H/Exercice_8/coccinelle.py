from coccinelle_pack import Window, Canvas, DirectionButtons, PhotoImage


if __name__ == '__main__':
    win = Window()
    canvas = Canvas(master=win)
    coc_up = PhotoImage(file='cocH.gif')
    coc_down = PhotoImage(file='cocB.gif')
    coc_left = PhotoImage(file='cocG.gif')
    coc_right = PhotoImage(file='cocD.gif')
    tags = 'coccinelle'
    canvas.create_image(0, 0, image=coc_right, anchor='nw', tags=tags)
    up = DirectionButtons(win, place_x=625, place_y=350, canvas=canvas, image=coc_up, tags=tags, text='Haut')
    down = DirectionButtons(win, place_x=625, place_y=450, canvas=canvas, image=coc_down, tags=tags, text='Bas')
    left = DirectionButtons(win, 575, 400, canvas, image=coc_left, tags=tags, text='Gauche')
    right = DirectionButtons(win, 675, 400, canvas, image=coc_right, tags=tags, text='Droite')

    canvas.focus_set()
    canvas.bind("<Up>", up.on_click)
    canvas.bind("<Down>", down.on_click)
    canvas.bind("<Right>", right.on_click)
    canvas.bind("<Left>", left.on_click)

    win.mainloop()
