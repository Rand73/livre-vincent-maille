from tools_tkinter import Window, Canvas, Button, PhotoImage


def swap():
    global gobelets

    for i in gobelets:
        gob: Gobelet = gobelets[i]
        gob.swap_place()

    gob_a: Gobelet = gobelets['gobeletA']
    gob_b: Gobelet = gobelets['gobeletB']
    while 1:
        a = gob_a.move()
        b = gob_b.move()
        if a and b:
            return
        pass
    pass


class Gobelet:
    __canvas: Canvas
    __x: int
    __y: int
    __tags: str
    __image: PhotoImage
    __place: str
    __dict_place = {'a': (100, 100),
                    'b': (350, 100)}

    def __init__(self, canvas: Canvas, place: str, tags: str, image: PhotoImage):
        self.__canvas = canvas
        self.__x, self.__y = self.__dict_place[place]
        self.__tags = tags
        self.__image = image
        self.place = place

        self.__canvas.create_image(self.__x, self.__y, image=self.__image, tags=self.__tags, anchor='nw')
        pass

    @property
    def place(self):
        return self.__place

    @place.setter
    def place(self, place):
        self.__place = place
        self.__x, self.__y = self.__dict_place[self.place]
        pass

    def swap_place(self):
        if self.place == 'a':
            self.place = 'b'
            return
        self.place = 'a'
        pass

    def __coords(self):
        return self.__canvas.coords(self.__tags)

    def move(self) -> bool:
        x, y = self.__coords()

        if x == self.__x:
            return True
        dx = 1
        dy = 0
        if self.place == 'a':
            self.__canvas.tag_raise(self.__tags)
            dx = -1
            if abs(x - self.__x) <= 125:
                dy = 1
            else:
                dy = -1

        can.move(self.__tags, dx * 5, dy)
        x, y = self.__coords()
        can.update()
        if x == self.__x:
            print((x, y))
            return True
        return False


if __name__ == '__main__':
    win = Window(width=600, height=430)
    win.title("Exercice 6")
    can = Canvas(master=win, width=600, height=400)
    can.pack()

    gobelet_a = PhotoImage(file='gobeletA.gif')
    gobelet_b = PhotoImage(file='gobeletB.gif')

    gobelets = {'gobeletA': Gobelet(can, 'a', 'gobeletA', gobelet_a),
                'gobeletB': Gobelet(can, 'b', 'gobeletB', gobelet_b)}

    but = Button(win, text='Echange', command=swap)
    but.pack()
    win.mainloop()
