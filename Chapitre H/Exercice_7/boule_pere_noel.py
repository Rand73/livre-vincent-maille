from tools_tkinter import Window, Canvas, Font, Button  # , ALL
from tkinter import PhotoImage
from random import randint


def advanced():
    global can, width
    for _ in range(width - 200):
        can.move('pere_noel', 1, 0)
        can.update()


if __name__ == '__main__':

    width = 1000
    height = 500
    canvas_bg = 'blue'

    win = Window(width=width, height=height + 40)
    win.title('Bataille de boules de neige')

    can = Canvas(master=win, width=width, height=height, bg=canvas_bg)
    can.pack()
    button_noel = Button(win, text='Avance', command=advanced)
    button_noel.pack()

    img = PhotoImage(file="noel.gif", name='noel')
    can.create_image(0, 200, image=img, anchor='nw', tags='pere_noel')

    # Création des lignes
    x1 = 0  # Point tout à gauche.
    x2 = width / 2  # Point au centre.
    x3 = width  # Point tout à droite.
    for i in range(0, 50, 5):
        y1 = i * 2
        y2 = i
        y3 = y1
        _ = can.create_line(x1, y1, x2, y2, fill='white')
        _ = can.create_line(x2, y2, x3, y3, fill='white')

    font = Font(family='helvetica', size=40)
    text = can.create_text(x2, 100, fill='yellow', text='Bataille de boules de neige !', font=font)

    for x in range(width):
        for y in range(height):
            rand = randint(0, 500000)
            if 100 > rand > 10:
                d = randint(5, 10)
                _ = can.create_oval(x - d, y - d, x + d, y + d, fill='white', outline='white')
                # _ = can.create_text(x, y, fill='yellow', text='d={0} x={1} y={2}'.format(d, x, y),
                #                   font=('helvetica', d))

    win.mainloop()
