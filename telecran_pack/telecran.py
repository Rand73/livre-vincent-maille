from tools_tkinter import Window, Canvas, Frame, Button as But
from tkinter.messagebox import showwarning
import tkinter
from math import fabs


class Window(Window):

    def __init__(self):
        super(Window, self).__init__(width=800, height=600)
        self.resizable(width=False, height=False)
        self.title("Télécran")


class Frame(Frame):

    def __init__(self, master, width: int, height: int, place_x: int, place_y: int, bg=''):
        super(Frame, self).__init__(master=master, width=width, height=height, bg=bg)
        self.pack_propagate(0)
        self.place(x=place_x, y=place_y)


class Canvas(Canvas):
    __canvas_bg: str = "white"
    __width: int = 500
    __height: int = 500
    __x: int = 50
    __y: int = 50
    __line_color: str = 'black'
    __x1: int = 2
    __y1: int = 2

    def __init__(self, master):
        self.__frame = Frame(master=master,
                             width=self.__width,
                             height=self.__height,
                             place_x=self.__x,
                             place_y=self.__y)

        super(Canvas, self).__init__(master=self.__frame,
                                     bg=self.__canvas_bg)

        self.pack(fill=tkinter.BOTH, expand=1)
        self.x1 = 2
        self.y1 = 2
        pass

    @property
    def line_color(self):
        return self.__line_color

    @line_color.setter
    def line_color(self, color: str):
        self.__line_color = color
        pass

    @property
    def x1(self):
        return self.__x1

    @x1.setter
    def x1(self, x1: int):
        if x1 == self.x1:
            return
        if x1 > (self.__width - 3) or x1 < 2:
            self.showwarning()
            return
        if fabs(x1 - self.x1) == 1:
            self.__x1 = x1
            return
        pass

    @property
    def y1(self):
        return self.__y1

    @y1.setter
    def y1(self, y1: int):
        if y1 == self.y1:
            return
        if y1 > (self.__height - 3) or y1 < 2:
            self.showwarning()
            return
        if fabs(y1 - self.y1) == 1:
            self.__y1 = y1
            return
        pass

    def create_line(self, direction: str):

        x2, y2 = self.outline(self.x1, self.y1, direction)
        x1 = self.x1
        y1 = self.y1
        xor = (x1 != x2) ^ (y1 != y2)
        if xor:
            self.x1 = x2
            self.y1 = y2
            return super().create_line(x1, y1, x2, y2, fill=self.line_color)
        pass

    def outline(self, x1: int, y1: int, direction: str):
        if direction == 'Haut':
            return x1, y1 - 1
        if direction == 'Bas':
            return x1, y1 + 1
        if direction == 'Gauche':
            return x1 - 1, y1
        if direction == 'Droite':
            return x1 + 1, y1
        pass

    def showwarning(self):
        showwarning(title='Dépassement de zone',
                    message="Attention, vous essayer de sortir de l'écran")
        pass


class DirectionButtons(But):
    __width: int = 100
    __height: int = 50
    __canvas: Canvas
    __direction: str

    def __init__(self, master, place_x: int, place_y: int, canvas: Canvas, cnf={}, **kw):
        self.__frame = Frame(master=master,
                             width=self.__width,
                             height=self.__height,
                             place_x=place_x,
                             place_y=place_y)

        self.canvas = canvas
        self.__direction = kw['text']
        super(DirectionButtons, self).__init__(master=self.__frame,
                                               command=self.on_click,
                                               cnf=cnf,
                                               **kw)

        self.pack(fill=tkinter.BOTH, expand=1)
        pass

    @property
    def canvas(self):
        return self.__canvas

    @canvas.setter
    def canvas(self, canvas: Canvas):
        if isinstance(canvas, Canvas):
            self.__canvas = canvas
        pass

    def on_click(self, event=None):
        self.canvas.create_line(self.__direction)


class ColorButtons(But):
    __width: int = 50
    __height: int = 50

    def __init__(self, master, place_x: int, place_y: int, color: str, canvas: Canvas, cnf={}, **kw):
        self.__frame = Frame(master=master,
                             width=self.__width,
                             height=self.__height,
                             place_x=place_x,
                             place_y=place_y)

        self.color = color
        self.canvas = canvas

        super(ColorButtons, self).__init__(master=self.__frame,
                                           bg=color,
                                           command=lambda: self.on_click(canvas),
                                           cnf=cnf,
                                           **kw)

        self.pack(fill=tkinter.BOTH, expand=1)

    @property
    def color(self):
        return self.__color

    @color.setter
    def color(self, color: str):
        self.__color = color
        pass

    @property
    def canvas(self):
        return self.__canvas

    @canvas.setter
    def canvas(self, canvas):
        if isinstance(canvas, Canvas):
            self.__canvas = canvas
            return;
        pass

    def on_click(self, canvas: Canvas):
        canvas.line_color = self.color
        pass
