from random import randint


def nb_letters(mot: str, prop: str) -> int:
    mot = mot.upper()
    prop = prop.upper()
    for letter in prop:
        if letter in prop:
            if prop.count(letter) > mot.count(letter):
                return 0
            continue
        return 0

    return len(prop)


def choose_word():
    file = open("fichier/mot5.txt", 'r')
    mots = file.readlines()
    file.close()

    number = randint(1, len(mots))
    return mots[number].replace('\n', '')


def affiche_mot(m: str, l: str) -> str:
    response = ""
    for c in m:
        if c in l:
            response = response + c.upper() + " "
            continue
        response = response + "_ "
    return response


def nb_vies(m: str, l: chr, n: int) -> int:
    if m.find(l) == -1:
        return n - 1
    return n


while True:
    print("**********Bienvenue dans le jeu du pendu**********")
    print("Choix du mot dans la base de donnée.....")
    word = choose_word()
    print("Initialisation du nombre de vies")
    vies = 8
    display = affiche_mot(word, word[0])
    responses = "" + word[0]
    while vies > 0 and nb_letters(word, responses) != len(word):
        print(display)
        print("Il vous reste {0} vies.".format(vies))
        letter = input("Choississez une lettre:>>")
        if letter in responses:
            print("Vous avez déjà entré cette lettre, veuillez recommencer")
            continue
        responses += letter
        display = affiche_mot(word, responses)
        if display.replace(' ', '') == word.upper():
            break
        vies = nb_vies(word, letter, vies)
    print(affiche_mot(word, responses))
    if vies <= 0:
        print("Hélas tu as perdu, rejoue encore et essai de gagner")
        print("La réponse était : {0}".format(word.upper()))
    if vies > 0:
        print("Bravo tu as trouvé la réponse!!!!")
    print("*" * 50)

    if input("\nVeux tu rejouer? (Y/N):>>").upper() == 'Y':
        continue
    break
