from typing import List
from random import randint, choice
from tkinter.font import Font
from piano_1 import Window, Button, LabelTitle, BlackButton, KeyButton, LabelText, LabelToPlay, LabelKeyPressed


def choose_keys(keys: List[str]) -> List[str]:
    """

    :param keys:
    :return: List of music keys.
    """
    key_list: List[str] = []
    nb_key = randint(5, 10)
    for _ in range(nb_key):
        key_list.append(choice(keys))

    return key_list


if __name__ == "__main__":
    keys: List[str] = ["Do", "Ré", "Mi", "Fa", "Sol", "La", "Si"]
    keys_to_play: List[str] = choose_keys(keys)

    black_x: List[int] = [79, 132, 238, 291, 344]
    keys_button: List[KeyButton] = []
    window = Window()

    helvetica_24 = Font(family='helvetica', size=20)
    helvetica_18 = Font(family='helvetica', size=12)
    helvetica_36 = Font(family='helvetica', size=30)

    # Bouton quitter
    button = Button(master=window, text="Quitter", command=window.destroy).place(x=0, y=0)

    # Création des labels
    label_titre = LabelTitle(master=window, font=helvetica_24)
    _ = LabelText(master=window, font=helvetica_24)
    label_to_play = LabelToPlay(master=window,
                                font_label=helvetica_18,
                                text=keys_to_play)
    label_key_pressed = LabelKeyPressed(master=window, font_label=helvetica_36)

    # Construction des touches blanches
    for (x, key) in enumerate(keys):
        keys_button.append(KeyButton(master=window,
                                     place_x=(50 + (x * 53)),
                                     key=key,
                                     label_key_pressed=label_key_pressed,
                                     label_to_play=label_to_play))

    # Construction des touches noir
    for x in black_x:
        _ = BlackButton(master=window, place_x=x)

    window.mainloop()
