"""
    Fichier : pendu_2.py
    Auteur : Cédric Brun
    Créer le 20/03/2019
    Description : Correspond à l'exercice PENDU 2 page 96 du livre Apprendre à programmer par le jeu de Vincent Maille
"""
import sys

from pendu_pack import *


def main(argv=None):
    if argv is None:
        argv = sys.argv
    _ = Pendu(content="./fichier/pendu/")


if __name__ == "__main__":
    main()
