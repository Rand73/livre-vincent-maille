from courbes_pack import Window, Graph
from math import sin, cos, pi


class Astroide:

    def __init__(self):
        self.__win = Window("Astroïde")
        self.__graph = Graph(self.__win)

        for j in range(int(- pi * 1000), int(pi * 1000), 1):
            i = j / 1000
            i2 = i + 1 / 1000

            self.__graph.create_line(x1=self.func_x(i), y1=self.func_y(i), x2=self.func_x(i2), y2=self.func_y(i2))
        self.__win.mainloop()
        pass

    def func_y(self, t):
        return sin(t) ** 3

    def func_x(self, t):
        return cos(t) ** 3
