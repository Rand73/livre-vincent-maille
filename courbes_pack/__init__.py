from courbes_pack.base import *
from courbes_pack.lissajous import Lissajous
from courbes_pack.spirale_archimede import Archimede
from courbes_pack.astroide import Astroide
from courbes_pack.bicorne import Bicorne
from courbes_pack.bifolium_regulier import BifoliumRegulier
from courbes_pack.coeur import Coeur
