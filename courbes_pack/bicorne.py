from courbes_pack import Curve
from math import sin, cos, pi


class Bicorne(Curve):

    def __init__(self):
        super(Bicorne, self).__init__(name="Bicorne", range_min=-2 * pi, range_max=2 * pi, range_offset=1, multiplier=2)
        pass

    def func_x(self, t):
        return sin(t)

    def func_y(self, t):
        return (cos(t) ** 2) / (2 - cos(t))
