from tools_tkinter import Window, Canvas
from math import pi
from abc import abstractmethod


class Window(Window):

    def __init__(self, title: str):
        super(Window, self).__init__(width=600, height=600)
        self.resizable(width=False, height=False)
        self.title(title)
        pass


class Graph(Canvas):

    def __init__(self, master):
        super(Graph, self).__init__(master=master,
                                    width=500,
                                    height=500,
                                    bg='white')
        self.pack(padx=40, pady=40)

        # Création de la base du graph
        self.create_line(-1.1, 0, 1.1, 0)
        self.create_line(0, -1.1, 0, 1.1)
        # Placement du zéro
        self.create_text(0.01, -0.01, text='0', anchor='nw')

        # Placement du point y = 1
        self.create_line(0.05, 1, -0.05, 1)
        self.create_text(-0.06, 1, text='1', anchor='e')

        # Placement du point y = -1
        self.create_line(0.05, -1, -0.05, -1)
        self.create_text(-0.06, -1, text='-1', anchor='e')

        # Placement du point x = 1
        self.create_line(1, 0.05, 1, -0.05)
        self.create_text(1, -0.06, text='1', anchor='n')

        # Placement du point x = -1
        self.create_line(-1, 0.05, -1, -0.05)
        self.create_text(-1, -0.06, text='-1', anchor='n')

        pass

    def create_text(self, x, y, text, anchor, fill='black'):
        x, y = self.__zero_offset(x, y)
        return super().create_text(x, y, fill=fill, text=text, anchor=anchor, font=('helvetica', 15))

    def create_line(self, x1, y1, x2, y2, fill='black'):
        x1, y1 = self.__zero_offset(x1, y1)
        x2, y2 = self.__zero_offset(x2, y2)
        return super().create_line(x1, y1, x2, y2, fill=fill)

    def __zero_offset(self, x, y):
        """
        Méthode pour gérer l'offset entre un graphique (min = -1, max = 1) et le canvas 500 * 500
        :param x: x (-1.1 < x < 1.1)
        :param y: y (-1.1 < y < 1.1)
        :return:
        """
        return (x * 200) + 250, (-y * 200) + 250


class Curve:

    def __init__(self, name: str = "Curve", range_min=-pi, range_max=pi, range_offset: int = 1, multiplier: int = 1000):
        """
        Défini une courbe paramétrée pour
        x(t) = func_x(t) et y(t) = func_y(t) et pour t ∈ [min_range; max_range; range_offset].

        :param name: Nom de la courbe
        :param range_min: t min (-pi de base, convient à toutes les courbes définies par des cos/sin)
        :param range_max: t max (idem min)
        :param range_offset: int: offset (1 mini de base) (dans les négatifs pour un range_min > range_max)
        :param multiplier:  Coefficient multiplicateur qui donne la précision de la courbe
                            (plus il est grand plus la courbe est précise)
        """
        self.__win = Window(name)
        self.__graph = Graph(master=self.__win)

        for j in range(int(range_min * multiplier), int(range_max * multiplier), range_offset):
            i = j / multiplier
            i2 = i + range_offset / multiplier

            self.__graph.create_line(x1=self.func_x(i), y1=self.func_y(i), x2=self.func_x(i2), y2=self.func_y(i2))
        self.__win.mainloop()
        pass

    @abstractmethod
    def func_x(self, t):
        """
        Méthode à surcharger dans une classe enfant plus spécifique dans laquelle on retournera la fonction
        x spécifique à la courbe.
        Exemple pour une courbe Astroïde : return cos(t) ** 3
        :param t:
        """
        raise NotImplementedError('User must define "func_x" method to use this class')

    @abstractmethod
    def func_y(self, t):
        """
        Méthode à surcharger dans une classe enfant plus spécifique dans laquelle on retournera la fonction
        y spécifique à la courbe.
        Exemple pour une courbe Astroïde : return sin(t) ** 3
        :param t:
        """
        raise NotImplementedError('User must define "func_x" method to use this class')
