from courbes_pack import Curve
from math import sin, cos, pi


class BifoliumRegulier(Curve):

    def __init__(self):
        super(BifoliumRegulier, self).__init__(name="Bifolium Régulier", range_min=0, range_max=2 * pi)
        pass

    def func_x(self, t):
        return sin(t) * (1 - cos(t))

    def func_y(self, t):
        return sin(t) ** 2


# Test

if __name__ == "__main__":
    _ = BifoliumRegulier()
