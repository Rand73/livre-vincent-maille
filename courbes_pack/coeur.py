from courbes_pack import Curve
from math import sin, cos


class Coeur(Curve):

    def __init__(self):
        super(Coeur, self).__init__(name="Coeur", multiplier=10)
        pass

    def func_x(self, t):
        return 16 * (sin(t) ** 3) / 20

    def func_y(self, t):
        return (13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t)) / 20


if __name__ == "__main__":
    _ = Coeur()
