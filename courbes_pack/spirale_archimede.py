from courbes_pack import Window, Graph
from math import sin, cos


class Archimede:
    __win: Window
    __graph: Graph

    def __init__(self):
        self.__win = Window("Spirale d'Archimède")
        self.__graph = Graph(master=self.__win)
        for j in range(0, 40 * 100, 1):
            i = j / 100
            i2 = i + 1 / 100
            self.__graph.create_line(x1=self.func_x(i), y1=self.func_y(i), x2=self.func_x(i2), y2=self.func_y(i2))
        self.__win.mainloop()
        pass

    def func_x(self, t):
        return (t * cos(t)) / 40

    def func_y(self, t):
        return (t * sin(t)) / 40
