from courbes_pack import Window, Graph
from math import cos, sin, pi


class Lissajous:
    __win: Window
    __graph: Graph

    def __init__(self):
        self.__win = Window('Courbe de Lissajous')
        self.__graph = Graph(self.__win)

        for j in range(int(-1000 * pi), int(1000 * pi), 1):
            i = j / 1000
            i2 = (j + 1) / 1000
            self.__graph.create_line(self.func_x(i), self.func_y(i), self.func_x(i2), self.func_y(i2))
        self.__win.mainloop()
        pass

    def func_x(self, t):
        return sin(2 * t)

    def func_y(self, t):
        return cos(3 * t)
