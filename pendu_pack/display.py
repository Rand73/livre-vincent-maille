"""
    Fichier : display.py
    Auteur : Cédric BRUN
    Créer le 01/04/2019
    Description : Contiendra toutes les parties concernant l'affichage du package pendu_pack.
"""

from tools_tkinter import Window as Win, PhotoImage, Button, Canvas as Can, Entry, Frame as Fra, BOTH, CENTER, Label
from tools_tkinter import StringVar


class Window(Win):
    """ Window for pendu_pack. """

    def __init__(self):
        """ Constructor for Window. """
        super(Window, self).__init__(width=800, height=400)
        self.title("Pendu")
        pass


class Frame(Fra):
    """ Frame for pendu_pack. """

    def __init__(self, win: Window, width: int, height: int, x: int, y: int):
        """Constructor for Frame"""
        super(Frame, self).__init__(master=win, width=width, height=height)
        self.pack_propagate(0)
        self.place(x=x, y=y)
        pass


class Canvas(Can):
    """ Define the canvas for pendu app. """

    __width = 300
    __height = 300
    __x = 450
    __y = 50

    def __init__(self, win: Window):
        frame_can = Frame(win=win,
                          width=self.__width,
                          height=self.__height,
                          x=self.__x,
                          y=self.__y)

        super(Canvas, self).__init__(master=frame_can)
        self.pack(fill=BOTH, expand=1)
        pass

    pass


class SendButton(Button):
    """ Define the button for send a letter to check if it is present in the word. """

    __width = 150
    __height = 25
    __x = 250
    __y = 225
    __text = "Proposition"

    def __init__(self, win: Window, command):
        frame_btn = Frame(win=win,
                          width=self.__width,
                          height=self.__height,
                          x=self.__x,
                          y=self.__y)
        super(SendButton, self).__init__(master=frame_btn, text=self.__text, command=command)
        self.pack(fill=BOTH, expand=1)
        pass

    pass


class SendEntry(Entry):
    """ Define the Entry point for the user. """

    __width = 150
    __height = 25
    __x = 50
    __y = 225

    def __init__(self, win: Window, entry_text: StringVar):
        frame_entry = Frame(win=win,
                            width=self.__width,
                            height=self.__height,
                            x=self.__x,
                            y=self.__y)
        super(SendEntry, self).__init__(master=frame_entry, textvariable=entry_text, justify=CENTER)
        self.pack(fill=BOTH, expand=1)
        entry_text.set("")
        self.focus()
        pass

    pass
