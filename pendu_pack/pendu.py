"""
    Fichier : pendu.py.
    Auteur : Cédric BRUN.
    Créer le 2019-04-01.
    Description : Package pendu pour l'exercice pendu sur le livre de vincent maille.
"""

from typing import Dict
from tools_tkinter import PhotoImage, Frame, BOTH, CENTER, Label, Entry
from pendu_pack.display import Window, SendButton, Canvas, SendEntry
from tools_tkinter import StringVar
from tkinter.messagebox import showinfo
from random import randint
from unidecode import unidecode


class Pendu:
    """Définit le programme pendu"""

    __win: Window
    __folder: str
    __images: Dict[int, PhotoImage] = {}
    __images_id = 1
    __tags: str = 'pendu'

    __bg_color: (int, int, int)

    __btn_proposition: SendButton

    __canvas: Canvas

    __entry_letter: Entry
    __entry_text: StringVar

    __label_mot: Label
    __label_mot_text: StringVar
    __mot_a_trouver: str
    __propositions: str
    __label_info: Label
    __label_info_text: StringVar

    __nb_chances: int

    def __init__(self, content: str = "../fichier/pendu/"):
        """Constructor for Pendu"""
        self.__folder = content
        self.__win = Window()
        self.__init_images()
        self.__win.configure(bg=self.__bg_color)

        self.__btn_proposition = SendButton(win=self.__win, command=self.__command_button)

        self.__canvas = Canvas(win=self.__win)

        self.__init_canvas()

        self.__entry_text = StringVar()
        self.__entry_letter = SendEntry(self.__win, entry_text=self.__entry_text)
        self.__entry_letter.focus_set()
        self.__entry_letter.bind("<Return>", func=self.__command_button)
        self.__nb_chances = 7
        self.__propositions = ""
        self.__init_labels()

        self.__win.mainloop()
        pass

    def __init_images(self, ):
        """Initialise le dictionnaire de gifs"""
        for i in range(1, 9):
            file = self.__folder + 'bonhomme' + str(i) + '.gif'
            self.__images[i] = PhotoImage(file=file)
        self.__bg_color = '#%02x%02x%02x' % self.__images[1].get(0, 0)
        print(self.__bg_color)
        pass

    def __init_frame(self, width: int, height: int, x: int, y: int) -> Frame:
        frame = Frame(master=self.__win, width=width, height=height)
        frame.pack_propagate(0)
        frame.place(x=x, y=y)
        return frame
        pass

    def __init_canvas(self, ):
        """ Initialisation du canvas avec la première image du pendu"""
        self.__canvas.create_image(0, 0, image=self.__images[self.__images_id], anchor='nw', tags=self.__tags)
        pass

    def __init_labels(self):
        """ Initialisation du label_mot. """
        label_frame = self.__init_frame(width=350, height=45, x=50, y=50)

        self.__label_mot_text = StringVar()
        self.__label_mot_text.set('_' * 7)
        self.__label_mot = Label(master=label_frame,
                                 textvariable=self.__label_mot_text,
                                 justify=CENTER,
                                 bg=self.__bg_color,
                                 fg='white')
        self.__label_mot.pack(fill=BOTH, expand=1)

        label_mot_frame = self.__init_frame(width=350, height=20, x=50, y=300)

        self.__label_info_text = StringVar()
        self.__label_info = Label(master=label_mot_frame,
                                  textvariable=self.__label_info_text,
                                  justify=CENTER,
                                  bg=self.__bg_color,
                                  fg='white')
        self.__label_info.pack(fill=BOTH, expand=1)
        self.__label_info_text.set('Il vous reste {} essais'.format(self.__nb_chances))
        self.choose_word()
        pass

    def __command_button(self, _=None):
        """ Méthode exécutée lors de l'appuie sur le bouton ou sur la touche entrer. """

        if len(self.__entry_letter.get()) == 0:
            showinfo('Entrez une lettre dans l\'input', '')
            return

        if not self.resume_labels():
            self.__images_id += 1
            self.__nb_chances -= 1
            self.__label_info_text.set('Il vous reste {} essais'.format(self.__nb_chances))

        if self.__nb_chances == 0:
            showinfo("Perdu!", "Vous avez perdu, le mot était {}. \nAu revoir!!".format(self.__mot_a_trouver))
            self.__win.quit()
            return

        if self.__mot_a_trouver == self.__label_mot_text.get():
            showinfo('Bravo', self.__mot_a_trouver)
            self.__win.quit()

        image = self.__images.get(self.__images_id)

        if image:
            self.__canvas.itemconfigure(self.__tags, image=image)

        pass

    def resume_labels(self):
        mot: str = ""
        letter: str = self.__entry_letter.get()
        self.__entry_text.set('')
        if len(letter) > 0:
            letter = letter[0]
        if letter in self.__propositions:
            showinfo('Cette lettre à déjà été proposée', '')
            return True
        self.__propositions += letter
        if letter not in self.__mot_a_trouver:
            return False

        for l in unidecode(self.__mot_a_trouver):
            if l in self.__propositions:
                mot += l
            else:
                mot += '_'

        self.__label_mot_text.set(mot)

        return True

    def choose_word(self):
        mots: list = []
        with open(self.__folder + "mot7.txt", 'r') as file:
            mots = file.readlines()

        number = randint(1, len(mots))
        self.__mot_a_trouver = mots[number].replace('\n', '')


if __name__ == "__main__":
    _ = Pendu()
