from random import randint
from time import sleep
from tkinter import *
from tkinter import Button
from typing import List, Callable, Any

result: int = 0
total: int = 0
buttons: List[Button] = []


def init():
    global total
    global result
    total = 0
    result = 0
    print('init')
    while total < 10:
        total = 0
        for num, button in enumerate(buttons):
            button.config(text="{0}".format(randint(1, 9)))
            print(button['text'])
            if randint(0, 4) > 1:
                total += int(button['text'])
            button.place(x=(num + 1) * 40, y=40)

    label_total_to_reach.config(text="Total à atteindre : {0}".format(total))
    label_current_total.config(text="Total actuel : {0}".format(result))
    fenetre.update()


def update(number: int):
    global result, total
    result += number
    if result == total:
        label_current_total.config(
            text="FELICITATIONS LE COMPTE EST BON : {0}. Restart pour recommencer.".format(result))
        return
    label_current_total.config(text="Total actuel : {0}".format(result))


def do_click(btn: Button):
    """

    :type btn: Button
    """
    update(int(btn['text']))
    btn.place_forget()


def create_button():
    """
        Création des boutons
    :return:
    """
    height: int = 2
    width = 4
    btn = Button(master=fenetre, height=height, width=width)
    click: Callable[[], None] = lambda: do_click(btn)
    btn.config(command=click)
    return btn


# Création de la window
fenetre = Tk()
fenetre.geometry("400x200+510+300")
fenetre.title("Comptez bon!")

# Création des boutons


for i in range(5):
    buttons.append(create_button())

button_quit = Button(master=fenetre, text="Quitter", height=2, width=8)
button_quit.config(command=fenetre.destroy)
button_quit.place(x=310, y=150)

button_restart = Button(master=fenetre, text="Restart", height=2, width=8)
button_restart.config(command=init)
button_restart.place(x=210, y=150)

# Création des labels
label_total_to_reach = Label(master=fenetre)
label_total_to_reach.place(x=40, y=100)

label_current_total = Label(master=fenetre)
label_current_total.place(x=40, y=126)

init()
fenetre.mainloop()
