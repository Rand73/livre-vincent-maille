from files.file import File


class WriteFile(File):
    """Liste de méthodes servant à l'écriture dans les fichiers"""

    def __init__(self, path):
        super(WriteFile, self).__init__(path)
        return

    def append(self, text):
        """Ouvre le fichier et lui ajoute la ligne contenue dans le parametre text."""
        with open(self._path, 'a') as file:
            file.write(text)
        return

    def append_csv(self, text):
        """Ajoute un ';' au text pour se conformer au format '.csv', et appelle la méthode append()."""
        self.append('{0};'.format(text))
