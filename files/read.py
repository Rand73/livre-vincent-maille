from files.file import File
from typing import List


class ReadFile(File):

    def __init__(self, path: str):
        """

        :type path: str
        """
        super(ReadFile, self).__init__(path=path)
        return

    def read(self, path=None) -> str:
        file = None
        try:
            if path:
                file = open(path, 'r')
            if path is None:
                file = open(self._path, 'r')

            ret = file.read().replace('\n', '')
        finally:
            file.close()
        return ret

    def read_to_array(self) -> List[str]:
        with open(self._path, 'r') as file:
            return file.read().split('\n')
