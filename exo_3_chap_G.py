from tkinter import *
from string_utils import shuffle
from random import randint
from files.read import ReadFile
from typing import List
import time

words_list: List[str] = ReadFile("fichier/mots.txt").read_to_array()
mot = ''


def melange_mot(mot: str) -> str:
    return shuffle(mot)


def choose_word() -> str:
    global mot
    mot = words_list[randint(0, len(words_list))]
    print(mot)
    return melange_mot(mot).upper()


def init():
    label.config(text=choose_word())
    entry.focus()


def do_click():
    if entry.get().upper()== mot.upper():
        label.config(text='BRAVO!!!!')
        window.update()
        time.sleep(2)
        window.destroy()
        return
    label.config(text='Perdu, recommence!!!')
    window.update()
    time.sleep(2)
    init()



window = Tk()
window.title('Mots Mélangés')
window.geometry("400x200+510+300")

button_check = Button(master=window, text='Valider', command=do_click, height=2, width=10)
button_check.place(x=145, y=145)

entry = Entry(master=window, width=20)
entry.place(x=100, y=77)

label = Label(master=window, width= 20)
label.place(x=100, y=51)
init()
print(label.cget('text'))
window.mainloop()
