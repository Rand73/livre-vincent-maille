import tkinter


class Button(tkinter.Button):

    def __init__(self, master=None, cnf={}, **kw):
        super(Button, self).__init__(master=master, cnf=cnf, **kw)
