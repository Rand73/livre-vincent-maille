import tkinter


class Label(tkinter.Label):

    def __init__(self, master=None, cnf=None, **kw):
        if cnf is None:
            cnf = {}
        super(Label, self).__init__(master=master, cnf=cnf, **kw)
