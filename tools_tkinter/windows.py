import tkinter


class Window(tkinter.Tk):

    def __init__(self, width: int = 200, height: int = 200):
        """
        Init with size of window. The window open on the center of the screen.
        :param width: largeur de la fenêtre
        :param height: hauteur de la fenêtre
        """
        super(Window, self).__init__()
        self.window_centering(width=width, height=height)

    def window_centering(self, width: int = 200, height: int = 200):
        """
        Calculate the geometry for center the window on the screen
        :param width:
        :param height:
        """
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        x = (screen_width - width) / 2
        y = (screen_height - height) / 2
        geo = "{}x{}+{}+{}".format(width, height, int(x), int(y))
        self.geometry(geo)
        pass
