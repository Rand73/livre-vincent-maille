from tkinter import *
from random import randint

# Constantes
TEXT_STONE: str = 'Pierre'
TEXT_PAPER: str = 'Feuille'
TEXT_SCISSORS: str = 'Ciseaux'
BUTTON_LIST = [TEXT_STONE, TEXT_PAPER, TEXT_SCISSORS]


# Exo 1: Jeu pierre feuille ciseaux

# Functions

def init():
    user_button_stone.place(x=30, y=40)
    user_button_paper.place(x=150, y=40)
    user_button_scissors.place(x=270, y=40)
    bot_button_stone.place(x=30, y=110)
    bot_button_paper.place(x=150, y=110)
    bot_button_scissors.place(x=270, y=110)
    label_dialogue.config(text="Faites votre choix !")


def winner(user_choice: str, bot: str) -> str:
    equal: str = "Egualité !"
    user_win: str = "Vous avez gagné !"
    bot_win: str = "L'ordinateur à gagné !"
    if user_choice == TEXT_STONE:
        if bot == TEXT_STONE:
            return equal
        if bot == TEXT_PAPER:
            return bot_win
        if bot == TEXT_SCISSORS:
            return user_win

    if user_choice == TEXT_PAPER:
        if bot == TEXT_STONE:
            return user_win
        if bot == TEXT_PAPER:
            return equal
        if bot == TEXT_SCISSORS:
            return bot_win

    if user_choice == TEXT_SCISSORS:
        if bot == TEXT_STONE:
            return bot_win
        if bot == TEXT_PAPER:
            return user_win
        if bot == TEXT_SCISSORS:
            return equal
    pass


def bot_choice(user_choice: str):
    bot = BUTTON_LIST[randint(0, 2)]
    if bot == TEXT_STONE:
        bot_push_stone()
    if bot == TEXT_PAPER:
        bot_push_paper()
    if bot == TEXT_SCISSORS:
        bot_push_scissors()
    label_dialogue.config(text=winner(user_choice, bot))


def button(text: str):
    HEIGHT = 2
    WIDTH = 8
    return Button(master=fen, text=text, height=HEIGHT, width=WIDTH)


def user_push_stone():
    globals()
    user_button_scissors.place_forget()
    user_button_paper.place_forget()
    bot_choice(TEXT_STONE)


def user_push_paper():
    user_button_scissors.place_forget()
    user_button_stone.place_forget()
    bot_choice(TEXT_PAPER)


def user_push_scissors():
    user_button_paper.place_forget()
    user_button_stone.place_forget()
    bot_choice(TEXT_SCISSORS)


def bot_push_stone():
    bot_button_paper.place_forget()
    bot_button_scissors.place_forget()


def bot_push_paper():
    bot_button_stone.place_forget()
    bot_button_scissors.place_forget()


def bot_push_scissors():
    bot_button_paper.place_forget()
    bot_button_stone.place_forget()


# Création/Initialisation de la window
fen = Tk()
fen.geometry("400x200+510+300")
fen.title("Pierre   -   Feuille   -   Ciseaux")

# Boutons de l'utilisateur

user_button_stone = button(TEXT_STONE)
user_button_stone.config(command=user_push_stone)

user_button_paper = button(TEXT_PAPER)
user_button_paper.config(command=user_push_paper)

user_button_scissors = button(TEXT_SCISSORS)
user_button_scissors.config(command=user_push_scissors)

# Boutons de l'ordinateur

bot_button_stone = button(TEXT_STONE)

bot_button_paper = button(TEXT_PAPER)

bot_button_scissors = button(TEXT_SCISSORS)

# Boutons Quitter et recommencer

button_quit = button("Quitter")
button_quit.config(command=fen.destroy)
button_quit.place(x=310, y=150)

button_restart = button("Restart")
button_restart.config(command=init)
button_restart.place(x=210, y=150)

# Création/Initialisation des labels

label_your_choice = Label(master=fen, text="Votre choix :")
label_your_choice.place(x=10, y=10)

label_my_choice = Label(master=fen, text="Mon choix :")
label_my_choice.place(x=10, y=80)

label_dialogue = Label(master=fen, text="Faites votre choix !")
label_dialogue.place(x=10, y=170)

init()
fen.mainloop()
