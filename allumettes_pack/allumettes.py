"""
    Fichier : allumettes.py.
    Auteur : Cédric BRUN.
    Créer le 02/09/2019.
    Description :   Jeux des Allumettes : Affiche entre 10 et 15 allumettes,
                    2 joueurs tirent entre 1 et 3 allumettes par
                    tour, celui à qui il reste 1 allumette à perdu.
"""


class Allumettes:
    """
    Gère le jeu des allumettes
    """

    def __init__(self, content: str = "../fichier/Allumettes/"):
        """Constructor for Allumettes"""
        self.__file: str = content
