from string_utils import shuffle

'''
    Exo 1: Mots Mélangés: Creer une fonction melange_mot(mot):
    Paramètre d'entrée:
        * mot: str
    Sortie:
        * Le même mot, mais avec les lettres mélangées
'''


def melange_mot(mot: str) -> str:
    return shuffle(mot)


# Test de la fonction melange_mot
print('Test de la fonction melange_mot avec le mot test (3 répétitions)')
melange_mot('test')
melange_mot('test')
melange_mot('test')

'''

    Exo 2: Scrabble: Créez une fonction nb_lettres(main, prop):
    Paramètres d'entrée:
        * main: Une chaîne composée de 7 lettres du joueur.
        * prop: Le mot proposé par le joueur.
    En sortie:
        * Le nombre de lettres de la proposition du joueur ou 0 si le mot est impossible à réaliser avec les lettres du jeu
        
'''


def nb_letters(mot: str, prop: str) -> int:
    mot = mot.upper()
    prop = prop.upper()
    if len(prop) > 7:
        return 0
    for letter in prop:
        if letter in prop:
            if prop.count(letter) > mot.count(letter):
                return 0
            continue
        return 0

    return len(prop)


# Test de la fonction nb_lettres
print("Test de la fonction nb_lettres avec les exemples du livre")
main = 'NBJORUO'
print(nb_letters(main, 'BONJOUR'))
print(nb_letters(main, 'JOUR'))
print(nb_letters(main, 'CHAT'))

'''

    Exo 3: Pendu: Créez une fonction nb_vies(m,l,n):
    Paramètres d'entrée:
        * m: le mot à trouver.
        * l: une lettre proposée par le joueur.
        * n: Le nombre de vies qu'il reste au joueur avant sa proposition.
    En sortie: 
        * Le nombre de vies restant au joueur
        
'''


def nb_vies(m: str, l: chr, n: int) -> int:
    if m.find(l) == -1:
        return n - 1
    return n


# Test de la fonction nb_vies
print("Test de la fonction nb_vies avec les exemples du livre")
print(nb_vies('BARBECUE', 'I', 5))

'''

    Exo 4: Pendu: créer une fonction affiche_mot(m, l):
    Paramètres d'entrée:
        * m: Le mot à trouver.
        * l: Une chaine formée de toutes les lettres déjà proposées.
    En sortie:
        * Le mot à trouver avec des _ à la place des lettres qu'il rest à trouver
        
'''


def affiche_mot(m: str, l: str) -> str:
    response = ""
    for c in m:
        if c in l:
            response = response + c.upper() + " "
            continue
        response = response + "_ "
    return response


# Test de la fonction affiche_mot
print("Test de la fonction affiche_mot avec les exemples du livre")
print(affiche_mot('MAISON', 'ISMZT'))
