from typing import List
from tools_tkinter import *

import tkinter


class Window(Window):

    def __init__(self):
        super(Window, self).__init__(width=468, height=586)
        self.resizable(width=False, height=False)
        self.title("Leçon de Piano")


class Frame(Frame):

    def __init__(self, master, width: int, height: int, place_x: int, place_y: int, bg=''):
        super(Frame, self).__init__(master=master, width=width, height=height, bg=bg)
        self.pack_propagate(0)
        self.place(x=place_x, y=place_y)


class Button(Button):

    def __init__(self, master=None, cnf={}, **kw):
        super(Button, self).__init__(master=master, cnf=cnf, **kw)


class KeyButton:

    def __init__(self, master, place_x: int, key: str, label_key_pressed, label_to_play):
        self.frame = Frame(master=master, width=50, height=300, place_x=place_x, place_y=100)
        self.button = Button(master=self.frame,
                             command=lambda: self.__key_pressed(key),
                             activebackground='Yellow',
                             relief='flat',
                             bg="Gray")
        self.button.pack(fill=tkinter.BOTH, expand=1)
        self.__label_key_pressed = label_key_pressed
        self.__label_to_play: LabelToPlay = label_to_play

    def __key_pressed(self, key: str):
        self.__label_to_play.key_played(key=key, label_key_pressed=self.__label_key_pressed)


class BlackButton:

    def __init__(self, master, place_x: int):
        self.frame = Frame(master=master, width=45, height=200, place_x=place_x, place_y=100, bg='black')


class LabelTitle(Label):

    def __init__(self, master, font):
        super(Label, self).__init__(master=master, text="La leçon de Piano", font=font)
        self.place(x=131, y=10)


class LabelText:

    def __init__(self, master, font):
        self.frame = Frame(master=master, width=368, height=33, place_x=50, place_y=420)
        self.label = Label(master=self.frame, text="A jouer :", font=font)
        self.label.pack(fill=tkinter.BOTH, expand=1)


class LabelKeyPressed:
    __text: str
    __label: Label
    __text_color: str

    def __init__(self, master, font_label, text=''):
        self.__frame = Frame(master=master, width=54, height=50, place_x=211, place_y=516)
        self.__label = Label(master=self.__frame, font=font_label)
        self.__label.pack(fill=tkinter.BOTH, expand=1)
        self.change_text(text)

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, text=''):
        self.__text = text
        self.__label.configure(text=self.text)

    @property
    def text_color(self):
        return self.__text_color

    @text_color.setter
    def text_color(self, text_color):
        self.__text_color = text_color
        self.__label.configure(fg=self.text_color)

    def change_text(self, text: str, text_color: str = 'green'):
        self.text = text
        self.text_color = text_color
        pass


class LabelToPlay:
    __text: str
    __label: Label

    def __init__(self, master, text: List[str], font_label):
        self.__frame = Frame(master=master, width=368, height=25, place_x=50, place_y=472)
        self.__label = Label(master=self.__frame, font=font_label)
        self.__label.pack(fill=tkinter.BOTH, expand=1)
        self.text = text

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, text):
        if isinstance(text, List):
            self.__text = " - ".join(text)
        if isinstance(text, str):
            self.__text = text

        self.__label.configure(text=self.text)

    def key_played(self, key: str, label_key_pressed: LabelKeyPressed):
        key_list = self.text.split(' - ')
        if key == key_list[0]:
            label_key_pressed.change_text(text=key)
            key_list.pop(0)
            self.text = key_list
        else:
            label_key_pressed.change_text(text=key, text_color='red')
        if self.text == '':
            self.text = "Bravo tu as bien joué!!"

        pass


