from typing import List
from unittest import TestCase
from tkinter import font
from piano_1 import Window, LabelToPlay


class TestLabelToPlay(TestCase):
    def test_key_played(self):

        keys_to_play: List[str] = ['Do', 'Do', 'Ré', 'Mi', 'Sol', 'Fa', 'Mi', 'Sol', 'Do', 'La']
        window = Window()
        helvetica_18 = font.Font(family='helvetica', size=12)
        label = LabelToPlay(master=window, text=keys_to_play, font_label=helvetica_18)
        keys_to_play: List[str] = ['Do', 'Ré', 'Mi', 'Sol', 'Fa', 'Mi', 'Sol', 'Do', 'La']
        label.key_played('Do')
        self.assertEqual(label.text, " - ".join(keys_to_play))
