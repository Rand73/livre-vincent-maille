from random import randint


def verif(word: str, prop: str):
    ret = verif_place_ok(word, prop)
    ret = verif_mauvaise_place(word, prop, ret)
    return ret


def verif_place_ok(word: str, prop: str):
    ret = ''
    comp = -1
    for l in prop:
        comp += 1
        if l == word[comp]:
            ret = ret + l
            continue
        ret = ret + '.'
    return ret.upper()


def verif_mauvaise_place(word: str, prop: str, ret: str):
    comp = -1
    retour = ''
    for l in prop:
        comp += 1
        if ret[comp].isupper():
            retour = retour + ret[comp]
            continue
        if word.find(l):
            if word.count(l) > (ret[comp:len(ret)].lower().count(l) + retour.lower().count(l)):
                retour = retour + l
                continue
        retour = retour + ret[comp]
    return retour


def choose_word():
    file = open("fichier/mot7.txt", 'r')
    mots = file.readlines()
    file.close()

    nombre = randint(1, len(mots))
    return mots[nombre].replace('\n', '')


print("********** MOTUS **********")
word = choose_word()
proposition = 6
while proposition > 0:
    prop: str = input("Entrez une proposition (Essai N°{0}): ?".format(7 - proposition)).lower()
    if len(prop) == 7:
        if prop == word:
            print("Bravo tu as trouver le bon mot : '{0}'.".format(word.upper()))
            break
        print(verif(word, prop))
    else:
        print('........')
    proposition -= 1

if not word == prop:
    print("Perdu !! Le bon mot était : '{0}'.".format(word.upper()))