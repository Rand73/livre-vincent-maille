import random

from files.write import WriteFile
from files.read import ReadFile

"""Definition des constantes"""

FICHIER_JUSTE_PRIX = "fichier/juste_prix.csv"
save_file = WriteFile(FICHIER_JUSTE_PRIX)


def creationPrix():
    return random.randint(30, 100)


print("Bonjour, bienvenue dans le jeu du Juste Prix.")
print("Nous allons générer un prix entre 30Euro et 100 Euro.")
print("Votre but sera de le découvrir en utilisant le moins de chances possible (10 Max)")
print("Génération aléatoire du prix......")
print("Pour tricher vous pouver taper le code'123' lorsque vous choisissez le prix")
prix: int = creationPrix()
print(prix)

responses = ["C'est Plus",
             "C'est moins",
             "Vous avez gagné!!",
             "Attention, plus que cinq tentatives",
             "Vous avez utiliser le code triche, voici le juste prix : {0} Euros".format(str(prix)),
             "Voulez-vous rejouer? O/N",
             "Les dix essais sont passés, vous avez perdu le prix était : {0} Euros".format(str(prix))]


def compare(prix: int, prix_utilisateur: int):
    if prix > prix_utilisateur:
        response(0)
    if prix < prix_utilisateur:
        response(1)
    return prix == prix_utilisateur


def response(rep: int):
    print(responses[rep])


def moyen(statCoup):
    total = 0
    for i in statCoup:
        total += i
    return str(total / len(statCoup))


def pourcent(statCoup):
    return str(((len(statCoup) - statCoup.count(10)) / len(statCoup)) * 100)


# Statistiques
counter = 0
while (True):
    counter += 1
    if counter > 10:
        response(6)
        response(5)
        save_file.append_csv(counter)
        if input() == 'O':
            counter = 0
            prix: int = creationPrix()
            responses.pop()
            responses.append("Les dix essais sont passés, vous avez perdu le prix était : {0} Euros".format(str(prix)))
            continue
        break
    if counter == 6:
        response(3)

    print("Essai N°{0}".format(str(counter)))
    user_price = eval(input("Entre ton prix : ?"))
    if user_price == 123:
        response(4)
        user_price = eval(input("Entre le bon prix : ?"))
    if compare(prix, user_price):
        response(2)
        print("Il vous à fallut {0} tentative(s).".format(str(counter)))
        save_file.append_csv(counter)
        response(5)
        if input().upper() == 'O':
            counter = 0
            prix: int = creationPrix()
            responses.pop()
            responses.append("Les dix essais sont passés, vous avez perdu le prix était : {0} Euros".format(str(prix)))
            continue
        break
stat = ReadFile.read(FICHIER_JUSTE_PRIX)
stat = stat.split(';')
stat.remove('')
stat = [int(x) for x in stat]
print("Vous avez fait {0} parties".format(str(len(stat))))
print("Vous avez gagner {0} parties".format(str(len(stat) - stat.count(11))))
print("Votre nombre de coup moyen est : {0}".format(moyen(stat)))
print("Votre pourcentage de réussite est : {0}%".format(pourcent(stat)))

